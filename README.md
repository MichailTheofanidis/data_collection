Repository for Data Collection


The folder contains three catkin packages:

1) The service_pkg that contains services to capture data from the realsense and kinect cameras. Requires:

	Realsense Node: https://github.com/IntelRealSense/realsense-ros
	Kinect rosbridge: https://github.com/code-iai/iai_kinect2

2) The sensor-fusion package that contains:
	
	a) The camera_record script that provides a UI to control the recorder.
	b) A recorder class to synchronize the servided ofr service_pkg
	c) The camera_vis that visualizes the recorded data.
	d) The imu_vis script to visualize imu data  (requires: https://github.com/qleonardolp/xsens__driver-release) 

