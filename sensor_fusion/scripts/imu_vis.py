#!/usr/bin/env python

import rospy
import tf
from sensor_msgs.msg import Imu


def handle_pose(msg, data):
    print(msg)
    print('-----------')

    br = tf.TransformBroadcaster()
    br.sendTransform((data[1], 0, 1),
                     (msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w),
                     rospy.Time.now(),
                     data[0],
                     "world")


if __name__ == '__main__':

    # Start the Simulation
    sensors = SensorFusion()

    # Start Subscriber to get imu data
    for i in range(0, len(sensors.imu_names)):
        rospy.Subscriber(sensors.imu_names[i], Imu, handle_pose, [sensors.imu_names[i], i])

    rospy.spin()