#!/usr/bin/env python

import Tkinter as tk
import rospy
from recorder import Recorder

global id

# Topic that will be stored
topics = ["return_kinect_color",
          "return_kinect_depth",
          "return_camera_color",
          "return_camera_depth"]

# Directory of data
directory = '/home/tron/Data/'

# Name of the dataset
name = 'test'

# Initialize the Recorder and variables
recorder = Recorder(topics, directory, name)
id = 2


# Menu to start, resume, pause and kill the Recorder
def start():
    global id
    recorder.make_bag(id)
    id = id+1
    recorder.start()


def resume():
    global id
    recorder.make_bag(id)
    id = id + 1
    recorder.resume()


def pause():
    print('Wait to stop the rosbag')
    recorder.pause()
    rospy.sleep(rospy.Duration(1))
    recorder.stop_bag()


def kill():
    recorder.kill()
    rospy.sleep(rospy.Duration(0.5))
    recorder.stop_bag()
    root.destroy()


root = tk.Tk()
onbutton = tk.Button(root, text="Start", command=start)
onbutton.pack()
onbutton = tk.Button(root, text="Resume", command=resume)
onbutton.pack()
offbutton = tk.Button(root, text="Pause", command=pause)
offbutton.pack()
killbutton = tk.Button(root, text="EXIT", command=kill)
killbutton.pack()

root.mainloop()

print('Recorder finished')