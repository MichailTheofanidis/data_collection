#!/usr/bin/env python

import rospy
import threading
import rosbag
import cv2

from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from service_pkg.srv import *


class Recorder(threading.Thread):

    def __init__(self, topics, directory, name):

        super(Recorder, self).__init__()
        self.paused = True
        self.daemon = True
        self.state = threading.Condition()
        self.topics = topics
        self.directory = directory
        self.name = name
        self.bag = []
        rospy.init_node('recorder')

    def make_bag(self, id):

        final_directory = self.directory + self.name + '_' + str(id)
        self.bag = rosbag.Bag(final_directory, 'w')
        print('New rosbag at '+final_directory)

    def stop_bag(self):
        self.bag.close()
        print('Stop the rosbag')

    def run(self):

        self.resume()
        while True:
            with self.state:
                if self.paused:
                    self.state.wait()

            time = rospy.Time.now()
            for topic in self.topics:
                print(topic)

                s = rospy.ServiceProxy(topic, ReturnImages)

                img_msg = self.create_image_msg(s)

                self.bag.write(topic, img_msg, t=time)

    def create_image_msg(self, msg):

        img_msg = Image()

        img_msg.width = msg([]).width
        img_msg.height = msg([]).height
        img_msg.encoding = msg([]).encoding
        img_msg.data = msg([]).data

        return img_msg

    def resume(self):
        with self.state:
            self.paused = False
            self.state.notify()  # Unblock self if waiting.

    def pause(self):
        with self.state:
            self.paused = True  # Block self.

    def kill(self):
        with self.state:
            self.paused = True
            print('You can terminate the recording program')
            self.join()
