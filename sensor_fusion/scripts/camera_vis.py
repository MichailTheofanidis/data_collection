#!/usr/bin/env python

import rosbag
import glob
import cv2
import os
import numpy as np

from cv_bridge import CvBridge
from sensor_msgs.msg import Image


def create_image_msg(msg):

    img_data = Image()

    img_data.width = msg.width
    img_data.height = msg.height
    img_data.encoding = msg.encoding
    img_data.data = msg.data

    bridge = CvBridge()
    cv_image = bridge.imgmsg_to_cv2(img_data, "passthrough")
    cv_image_resized = cv2.resize(cv_image, (640, 480), interpolation=cv2.INTER_AREA)

    return cv_image_resized


def make_img_directory(directory):

    if not os.path.exists(directory):
        os.makedirs(directory)

    return directory


def store_img(directory, img, img_id):

    if not os.path.exists(directory):
        os.makedirs(directory)

    print(directory+'/frame_'+str(img_id)+'.jpg')
    cv2.imwrite(directory+'/frame_'+str(img_id)+'.jpg', img)


# Directory of data
directory = '/home/tron/Data/'

# Name of the files
name = 'test_'

# Topic that will be listened
topics = ["return_kinect_color",
          "return_kinect_depth",
          "return_camera_color",
          "return_camera_depth"]

# Load all the bags of the directory
bags_dir = []
for bag_dir in glob.glob(directory + name + '*'):
    bags_dir.append(bag_dir)

# Iterate through every bag
threshold = len(topics)

for bag_dir in bags_dir:
    print(bag_dir)
    bag = rosbag.Bag(bag_dir)
    images = []
    counter = 0
    img_id = 0
    img_color = np.empty([480, 640, 3])
    img_depth = np.empty([480, 640])

    # Make image directory
    img_dir = make_img_directory(directory+'img_'+bag_dir[-1])

    for topic, msg, t in bag.read_messages():

        print("Topic:" + topic + " at time " + str(t))
        print('--------------------------')

        # Extract the image information
        img = create_image_msg(msg)

        # Store the raw image
        store_img(img_dir+'/' + topic, img, img_id)

        # Append all the images during a time frame
        images.append(img)
        counter = counter+1

        # Show all the images during a time frame
        if counter == threshold:

            img_color = np.array(images[0])
            img_depth = np.array(images[1])

            img_color = np.vstack((img_color, images[2]))
            img_depth = np.vstack((img_depth, images[3]))

            img_id = img_id + 1
            counter = 0
            images = []

        cv2.imshow("Color Image ", img_color)
        cv2.imshow("Depth Images", img_depth)

        cv2.waitKey(1)

    bag.close()


